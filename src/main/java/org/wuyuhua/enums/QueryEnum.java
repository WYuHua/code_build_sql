package org.wuyuhua.enums;

/**
 * 筛选条件枚举
 * @author 伍玉华
 */
public enum QueryEnum {
    // 不设置条件
    NULL(""),
    // =
    EQUALS("="),
    // !=
    NO_EQUALS("!="),
    // <
    LT("<"),
    // >
    GT(">"),
    // <=
    LTE("<="),
    // >=
    GTE(">="),
    // like '${param}'
    LIKE("like"),
    // like '% ${param}'
    R_LIKE("like", "%@"),
    // like '${param} %'
    L_LIKE("like", "@%"),
    // like '% ${param} %'
    A_LIKE("like", "%@%"),
    // not like '${param}'
    N_LIKE("not like"),
    // not like '% ${param}'
    N_R_LIKE("not like", "%@"),
    // not like '${param} %'
    N_L_LIKE("not like", "@%"),
    // not like '% ${param} %'
    N_A_LIKE("not like", "%@%"),
    // is null
    IS_NULL("is null"),
    // is not null
    N_NULL("is not null"),
    // = ''
    IS_BLANK("=", ""),
    // <> ''
    N_BLANK("<>", ""),
    // between
    BETWEEN("between"),
    // not between
    N_BETWEEN("not between"),
    // in
    IN("in","@"),
    // not in
    N_IN("not in","@");

    /**
     * 枚举值
     */
    public final String value;
    /**
     * 枚举参数 表示替换的方式 @表示参数
     */
    public final String param;

    QueryEnum(String value) {
        this.value = value;
        this.param = "@";
    }

    QueryEnum(String value, String param) {
        this.value = value;
        this.param = param;
    }
}
