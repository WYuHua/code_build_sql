package org.wuyuhua.pojo;

import java.util.List;

/**
 * <p>分组条件实体</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/4/26 17:38
 */

public class GroupByEntity {

    protected List<String> fieldNames;

    protected GroupByEntity next;

    private HavingEntity having;

    public GroupByEntity(List<String> names) {
        this.fieldNames = names;
    }

    public String print() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.join(",",fieldNames));
        GroupByEntity index = next;
        while (index != null){
            sb.append(",");
            sb.append(String.join(",",index.fieldNames));
            index = index.next;
        }
        sb.append("\n").append(having.print());
        return sb.toString();
    }

    public GroupByEntity having(Condition condition){
        HavingEntity havingEntity = new HavingEntity(condition);
        havingEntity.next = this.having;
        this.having = havingEntity;
        return this;
    }
}
