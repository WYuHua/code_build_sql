package org.wuyuhua.pojo;

import lombok.Data;
import org.wuyuhua.enums.QueryEnum;
import org.wuyuhua.inter.CustomFunction;

import java.util.Collection;

import static org.wuyuhua.enums.QueryEnum.*;

/**
 * <p>条件</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/4/24 15:24
 */
@Data
public class Condition {

    private ConditionNode root = null;
    private ConditionNode next = null;

    private Condition next(ConditionNode node){
        if (root == null) {
            root = node;
            next = root;
        } else {
            next.next(node);
            next = node;
        }
        return this;
    }

    public <T> Condition eq(CustomFunction<T,?> left,Object right){
        return condition(EQUALS,left,right);
    }
    public <T,R> Condition eq(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(EQUALS,left,right);
    }

    public <T> Condition neq(CustomFunction<T,?> left,Object right){
        return condition(NO_EQUALS,left,right);
    }
    public <T,R> Condition neq(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(NO_EQUALS,left,right);
    }

    public <T> Condition lt(CustomFunction<T,?> left,Object right){
        return condition(LT,left,right);
    }
    public <T,R> Condition lt(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(LT,left,right);
    }

    public <T> Condition gt(CustomFunction<T,?> left,Object right){
        return condition(GT,left,right);
    }
    public <T,R> Condition gt(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(GT,left,right);
    }

    public <T> Condition lte(CustomFunction<T,?> left,Object right){
        return condition(LTE,left,right);
    }
    public <T,R> Condition lte(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(LTE,left,right);
    }

    public <T> Condition gte(CustomFunction<T,?> left,Object right){
        return condition(GTE,left,right);
    }
    public <T,R> Condition gte(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(GTE,left,right);
    }

    public <T> Condition like(CustomFunction<T,?> left,Object right){
        return condition(LIKE,left,right);
    }
    public <T,R> Condition like(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(LIKE,left,right);
    }

    public <T> Condition rightLike(CustomFunction<T,?> left,Object right){
        return condition(R_LIKE,left,right);
    }
    public <T,R> Condition rightLike(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(R_LIKE,left,right);
    }

    public <T> Condition leftLike(CustomFunction<T,?> left,Object right){
        return condition(L_LIKE,left,right);
    }
    public <T,R> Condition leftLike(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(L_LIKE,left,right);
    }

    public <T> Condition allLike(CustomFunction<T,?> left,Object right){
        return condition(A_LIKE,left,right);
    }
    public <T,R> Condition allLike(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(A_LIKE,left,right);
    }

    public <T> Condition nLike(CustomFunction<T,?> left,Object right){
        return condition(N_LIKE,left,right);
    }
    public <T,R> Condition nLike(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(N_LIKE,left,right);
    }

    public <T> Condition nRightLike(CustomFunction<T,?> left,Object right){
        return condition(N_R_LIKE,left,right);
    }
    public <T,R> Condition nRightLike(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(N_R_LIKE,left,right);
    }

    public <T> Condition nLeftLike(CustomFunction<T,?> left,Object right){
        return condition(N_L_LIKE,left,right);
    }
    public <T,R> Condition nLeftLike(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(N_L_LIKE,left,right);
    }

    public <T> Condition nAllLike(CustomFunction<T,?> left,Object right){
        return condition(N_A_LIKE,left,right);
    }
    public <T,R> Condition nAllLike(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(N_A_LIKE,left,right);
    }

    public <T> Condition isNull(CustomFunction<T,?> left,Object right){
        return condition(IS_NULL,left,right);
    }
    public <T,R> Condition isNull(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(IS_NULL,left,right);
    }

    public <T> Condition notNull(CustomFunction<T,?> left,Object right){
        return condition(N_NULL,left,right);
    }
    public <T,R> Condition notNull(CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return condition(N_NULL,left,right);
    }

    public <T> Condition isBlank(CustomFunction<T,?> left){
        return condition(IS_BLANK,left);
    }

    public <T> Condition notBlank(CustomFunction<T,?> left){
        return condition(N_BLANK,left);
    }

    public <T> Condition betWeen(CustomFunction<T,?> left,Object right1,Object right2){
        return condition(BETWEEN,left,right1,right2);
    }

    @SafeVarargs
    public final <T,R> Condition betWeen(CustomFunction<T, ?> left, CustomFunction<R, ?>... right){
        return condition(BETWEEN,left,right);
    }

    public <T> Condition notBetWeen(CustomFunction<T,?> left,Object right1,Object right2){
        return condition(N_BETWEEN,left,right1,right2);
    }
    @SafeVarargs
    public final <T,R> Condition notBetWeen(CustomFunction<T, ?> left, CustomFunction<R, ?>... right){
        return condition(N_BETWEEN,left,right);
    }

    public <T> Condition in(CustomFunction<T,?> left,Object... right){
        return condition(IN,left,right);
    }

    public <T,E> Condition in(CustomFunction<T,?> left,Collection<E> rights){
        return condition(IN,left,rights);
    }

    @SafeVarargs
    public final <T,R> Condition in(CustomFunction<T, ?> left, CustomFunction<R, ?>... right){
        return condition(IN,left,right);
    }

    public <T> Condition notIn(CustomFunction<T,?> left,Object... right){
        return condition(N_IN,left,right);
    }

    public <T> Condition notIn(CustomFunction<T,?> left,Collection<?> rights){
        return condition(N_IN,left,rights);
    }
    @SafeVarargs
    public final <T,R> Condition notIn(CustomFunction<T, ?> left, CustomFunction<R, ?>... right){
        return condition(N_IN,left,right);
    }
    public <T> Condition condition(QueryEnum queryEnum, CustomFunction<T,?> left){
        return next(new ConditionNode(queryEnum, left));
    }

    public <T> Condition condition(QueryEnum queryEnum, String left){
        return next(new ConditionNode(queryEnum, left));
    }

    public <T> Condition condition(QueryEnum queryEnum, String left,Object right){
        return next(new ConditionNode(queryEnum,left,right));
    }

    public <R> Condition condition(QueryEnum queryEnum,String left,CustomFunction<R, ?> right){
        return next(new ConditionNode(queryEnum, left, right));
    }

    public Condition condition(QueryEnum queryEnum,String left,Object... right){
        return next(new ConditionNode(queryEnum, left, right));
    }

    @SafeVarargs
    public final <R> Condition condition(QueryEnum queryEnum, String left, CustomFunction<R, ?>... right){
        return next(new ConditionNode(queryEnum, left, right));
    }

    public <E> Condition condition(QueryEnum queryEnum, String left, Collection<E> right){
        return next(new ConditionNode(queryEnum, left, right));
    }

    public <T> Condition condition(QueryEnum queryEnum, CustomFunction<T,?> left, Object right){
        return next(new ConditionNode(queryEnum, left, right));
    }

    public <T,R> Condition condition(QueryEnum queryEnum,CustomFunction<T,?> left,CustomFunction<R, ?> right){
        return next(new ConditionNode(queryEnum, left, right));
    }

    public <T> Condition condition(QueryEnum queryEnum,CustomFunction<T,?> left,Object... right){
        return next(new ConditionNode(queryEnum, left, right));
    }

    @SafeVarargs
    public final <T,R> Condition condition(QueryEnum queryEnum, CustomFunction<T, ?> left, CustomFunction<R, ?>... right){
        return next(new ConditionNode(queryEnum, left, right));
    }

    public <T,E> Condition condition(QueryEnum queryEnum, CustomFunction<T,?> left, Collection<E> right){
        return next(new ConditionNode(queryEnum, left, right));
    }

    public Condition child(Condition child){
        ConditionNode conditionNode = new ConditionNode();
        conditionNode.child(child.root);
        if (this.root == null){
            this.root = conditionNode;
        }
        if (this.next != null){
            this.next.next(conditionNode);
        }
        this.next = conditionNode;
        return this;
    }

    public Condition and() {
        this.next.setType(0);
        return this;
    }

    public Condition or() {
        this.next.setType(1);
        return this;
    }

    public String print() {
        StringBuilder sb = new StringBuilder();
        print(this.root,sb);
        return sb.toString();
    }

    private void print(ConditionNode node, StringBuilder sb) {
        sb.append("(").append(node.print());
        if (node.getChild() != null) {
            print(node.getChild(), sb);
        }
        sb.append(")");
        if (node.getNext() != null) {
            sb.append(node.getType() == 1 ? " or " : " and ");
            print(node.getNext(), sb);
        }
    }
}
