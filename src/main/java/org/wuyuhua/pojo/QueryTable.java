package org.wuyuhua.pojo;

import org.wuyuhua.utils.ColumnUtil;
import org.wuyuhua.utils.StringUtil;

import javax.persistence.Table;

/**
 * <p>查询表实体</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/4/25 11:42
 */

public class QueryTable<T> {

    /**
     * 数据表
     */
    private Class<T> entity;

    protected final String tableName;

    /**
     * 别名
     */
    private String nickName;

    /**
     * 下一个表
     */
    protected QueryTable<?> next;

    /**
     * 连接类型 1.left join, 2.right join, 3 inner join
     */
    protected int nextType;

    /**
     * 连接条件
     */
    protected Condition condition;

    public QueryTable(Class<T> entity) {
        this.tableName = ColumnUtil.getTableName(entity);
    }


    public String print(){
        StringBuilder fromSb = new StringBuilder();
        QueryTable<?> preNextIndex = null;
        QueryTable<?> nextIndex = this;
        while (nextIndex != null) {
            fromSb.append(nextIndex.tableName);
            if (preNextIndex != null && preNextIndex.nextType > 0) {
                // 表示当前扫描的表不是根表
                fromSb.append(" on ").append(nextIndex.condition.print());
            }
            if (nextIndex.next != null) {
                switch (nextIndex.nextType) {
                    case 0:
                        fromSb.append(",");
                        break;
                    case 1:
                        fromSb.append(" left join ");
                        break;
                    case 2:
                        fromSb.append(" right join ");
                        break;
                    case 3:
                        fromSb.append(" inner join ");
                        break;
                    default:
                }
            }
            preNextIndex = nextIndex;
            nextIndex = nextIndex.next;
        }
        return fromSb.toString();
    }
}
