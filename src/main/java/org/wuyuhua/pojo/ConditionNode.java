package org.wuyuhua.pojo;

import com.alibaba.fastjson2.JSONObject;
import lombok.Data;
import org.wuyuhua.enums.QueryEnum;
import org.wuyuhua.inter.CustomFunction;
import org.wuyuhua.utils.ColumnUtil;
import org.wuyuhua.utils.ObjectUtil;
import org.wuyuhua.utils.StringUtil;

import javax.management.Query;
import java.util.*;

/**
 * <p>条件树节点</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * &#064;date  2024/4/23 15:01
 */
@Data
public class ConditionNode {

    private String left = "";
    private QueryEnum join = QueryEnum.NULL;
    private Collection<Object> right = null;
    private ConditionNode child;
    private ConditionNode next;
    private Boolean isFunction = false;
    private int type;

    public ConditionNode() {
    }

    public <T> ConditionNode(QueryEnum queryEnum, String left) {
        build(queryEnum, left);
    }

    public <T> ConditionNode(QueryEnum queryEnum, String left, Object right) {
        build(queryEnum, left, right);
    }

    public <T, R> ConditionNode(QueryEnum queryEnum, String left, CustomFunction<R, ?> r) {
        this.isFunction = true;
        build(queryEnum,left,ColumnUtil.getFieldName(r));
    }
    public <T> ConditionNode(QueryEnum queryEnum, String left, Object... right) {
        build(queryEnum,left, right);
    }

    @SafeVarargs
    public <T, R> ConditionNode(QueryEnum queryEnum, String left, CustomFunction<R, ?>... right) {
        this.isFunction = true;
        List<Object> r = new ArrayList<>();
        for (CustomFunction<R, ?> rCustomFunction : right) {
            r.add(ColumnUtil.getFieldName(rCustomFunction));
        }
        build(queryEnum, left, r);
    }

    public <T, E> ConditionNode(QueryEnum queryEnum, String left, Collection<E> collection) {
        build(queryEnum, left, collection);
    }
    public <T> ConditionNode(QueryEnum queryEnum, CustomFunction<T, ?> left) {
        build(queryEnum, ColumnUtil.getFieldName(left));
    }

    public <T> ConditionNode(QueryEnum queryEnum, CustomFunction<T, ?> left, Object right) {
        build(queryEnum, ColumnUtil.getFieldName(left), right);
    }

    public <T, R> ConditionNode(QueryEnum queryEnum, CustomFunction<T, ?> l, CustomFunction<R, ?> r) {
        this.isFunction = true;
        build(queryEnum, ColumnUtil.getFieldName(l), ColumnUtil.getFieldName(r));
    }

    public <T> ConditionNode(QueryEnum queryEnum, CustomFunction<T, ?> left, Object... right) {
        build(queryEnum, ColumnUtil.getFieldName(left), right);
    }

    @SafeVarargs
    public <T, R> ConditionNode(QueryEnum queryEnum, CustomFunction<T, ?> left, CustomFunction<R, ?>... right) {
        this.isFunction = true;
        List<Object> r = new ArrayList<>();
        for (CustomFunction<R, ?> rCustomFunction : right) {
            r.add(ColumnUtil.getFieldName(rCustomFunction));
        }
        build(queryEnum, ColumnUtil.getFieldName(left), r);
    }

    public <T, E> ConditionNode(QueryEnum queryEnum, CustomFunction<T, ?> left, Collection<E> collection) {
        build(queryEnum, ColumnUtil.getFieldName(left), collection);
    }

    public void next(ConditionNode node) {
        this.next = node;
    }

    public ConditionNode or(ConditionNode node) {
        this.type = 1;
        this.next = node;
        return this;
    }

    public void child(ConditionNode node) {
        this.child = node;
    }

    private void build(QueryEnum queryEnum, String left) {
        this.left = left;
        this.join = queryEnum;
    }

    private void build(QueryEnum queryEnum, String left, Object right) {
        build(queryEnum, left);
        this.right = new ArrayList<>();
        this.right.add(right);
    }

    private void build(QueryEnum queryEnum, String left, Object... right) {
        build(queryEnum, left);
        this.right = new ArrayList<>();
        for (Object object : right) {
            if (object instanceof Collection) {
                this.right.addAll((Collection<?>) object);
            } else {
                this.right.add(object);
            }
        }
        this.right.addAll(Arrays.asList(right));
    }


    @SuppressWarnings("unchecked")
    private <E> void build(QueryEnum queryEnum, String left, Collection<E> right) {
        build(queryEnum, left);
        this.right = (Collection<Object>) right;
    }

    public String print() {
        StringBuilder sb = new StringBuilder();
        if (join.equals(QueryEnum.NULL)) {
            return "";
        }
        sb.append(left).append(" ").append(join.value).append(" ");
        Iterator<?> iterator = right.iterator();
        if (right.size() == 1) {
            Object r = iterator.next();
            appendRight(sb, r);
        } else if (right.size() > 1) {
            switch (join) {
                case BETWEEN:
                case N_BETWEEN:
                    Object r1 = iterator.next();
                    Object r2 = iterator.next();
                    appendRight(sb, r1);
                    sb.append(" and ");
                    appendRight(sb, r2);
                    break;
                case IN:
                case N_IN:
                    sb.append("(");
                    while (iterator.hasNext()) {
                        appendRight(sb, iterator.next());
                        if (iterator.hasNext()) {
                            sb.append(",");
                        }
                    }
                    sb.append(")");
                default:
            }
        }
        return sb.toString();
    }

    private void appendRight(StringBuilder sb, Object r) {
        if (isFunction || r instanceof QueryEntity){
            sb.append(r);
            return;
        }
        if (ObjectUtil.isNotNumber(r) && !StringUtil.isEmpty(r.toString())) {
            try {
                Class.forName("com.alibaba.fastjson2.JSONObject");
                sb.append(JSONObject.toJSONString(r));
            } catch (ClassNotFoundException e) {
                sb.append("\"").append(r).append("\"");
            }
        } else {
            sb.append(r);
        }
    }
}
