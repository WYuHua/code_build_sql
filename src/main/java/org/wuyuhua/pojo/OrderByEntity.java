package org.wuyuhua.pojo;

import java.util.List;

/**
 * <p>排序条件实体</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/4/26 17:16
 */

public class OrderByEntity {

    protected List<String> fieldNames;
    /**
     * 1.aes 2.desc
     */
    protected int type = 1;

    protected OrderByEntity next;

    public OrderByEntity(List<String> fieldNames) {
        this.fieldNames = fieldNames;
    }

    public OrderByEntity(List<String> fieldNames,int type) {
        this.fieldNames = fieldNames;
        this.type = type;
    }

    public String print(){
        StringBuilder sb = new StringBuilder();
        sb.append(String.join(",",fieldNames));
        if (type == 1){
            sb.append(" aes ");
        } else {
            sb.append(" desc ");
        }
        OrderByEntity index = next;
        while (index != null){
            sb.append(",");
            sb.append(String.join(",",index.fieldNames));
            if (index.type == 1){
                sb.append(" aes ");
            } else {
                sb.append(" desc ");
            }
            index = index.next;
        }
        return sb.toString();
    }
}
