package org.wuyuhua.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import org.wuyuhua.annotation.TargetField;
import org.wuyuhua.enums.QueryEnum;
import org.wuyuhua.inter.CustomFunction;
import org.wuyuhua.utils.ColumnUtil;
import org.wuyuhua.utils.StringUtil;

import javax.persistence.Transient;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>查询实体</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/4/25 11:41
 */

public class QueryEntity {
    /**
     * 返回的列名 select ...
     */
    private final List<String> columns = new ArrayList<>();
    /**
     * 表
     */
    private QueryTable<?> tableRoot;
    private QueryTable<?> tableNext;
    private Condition conditionRoot;
    private LimitEntity limit;
    private OrderByEntity orderBy;
    private GroupByEntity groupBy;

    /**
     * 通过字符串构建select
     * @param columns 字符串
     * @return QueryEntity
     */
    public QueryEntity select(String... columns){
        for (String column : columns) {
            this.columns.add(StringUtil.convertCamelToSnake(column));
        }
        return this;
    }
    /**
     * 构建select
     *
     * @param functions 函数列表
     * @param <T>       泛型
     * @return this
     */
    @SafeVarargs
    public final <T> QueryEntity select(CustomFunction<T, ?>... functions) {
        for (CustomFunction<T, ?> function : functions) {
            ColumnUtil.CustomFunctionInfo functionInfo = ColumnUtil.getFunctionInfo(function);
            String tbName = ColumnUtil.getTableName(functionInfo.getClazz());
            addField(functionInfo.getField(),tbName);
        }
        return this;
    }

    /**
     * 构建select
     *
     * @param clazz 对象
     * @return this
     */
    public QueryEntity select(Class<?> clazz) {
        String tbName = ColumnUtil.getTableName(clazz);
        Field[] declaredFields = clazz.getDeclaredFields();
        addFields(declaredFields, tbName);

        Class<?> parent = clazz.getSuperclass();
        String pTableName = ColumnUtil.getTableName(parent);
        Field[] parentFields = parent.getDeclaredFields();
        addFields(parentFields, pTableName);
        return this;
    }

    /**
     * 使用类型构建查询表 类定义必须有@Table注解或@TableName注解标识绑定的是数据库中的那个表
     * @param clazz 类对象
     * @return QueryEntity
     * @param <T> 泛型
     */
    public <T> QueryEntity from(Class<T> clazz) {
        QueryTable<T> table = new QueryTable<>(clazz);
        if (tableRoot == null) {
            this.tableRoot = table;
        } else {
            this.tableNext.next = table;
        }
        this.tableNext = table;
        return this;
    }

    /**
     * 通过条件实体构建where
     * @param condition 条件实体
     * @return QueryEntity
     */
    public QueryEntity where(Condition condition) {
        this.conditionRoot = condition;
        return this;
    }


    public QueryEntity limit(int pageNo){
        return limit(pageNo,-1);
    }

    public QueryEntity limit(int pageNo,int pageNum){
        this.limit = new LimitEntity(pageNo,pageNum);
        return this;
    }

    /**
     * 构建 desc 排序
     * @param columns 待排序的列
     * @return QueryEntity
     * @param <T> 泛型
     */
    @SafeVarargs
    public final <T> QueryEntity orderByDesc(CustomFunction<T, ?>... columns){
        return orderBy(2,columns);
    }

    /**
     * 构建 aes 排序
     * @param columns 待排序的列
     * @return QueryEntity
     * @param <T> 泛型
     */
    @SafeVarargs
    public final <T> QueryEntity orderByAes(CustomFunction<T, ?>... columns){
        return orderBy(1,columns);
    }

    /**
     * 构建排序
     * @param type 类型 1.aes 2.desc
     * @param columns 待排序的列
     * @return QueryEntity
     * @param <T> 泛型
     */
    @SafeVarargs
    public final <T> QueryEntity orderBy(int type, CustomFunction<T, ?>... columns){
        List<String> fieldNames = new ArrayList<>();
        for (CustomFunction<T, ?> column : columns) {
            String fieldName = ColumnUtil.getFieldName(column);
            fieldNames.add(fieldName);
        }
        OrderByEntity orderByEntity = new OrderByEntity(fieldNames,type);
        if (null == this.orderBy){
            this.orderBy = orderByEntity;
        } else {
            OrderByEntity next = this.orderBy;
            while(next.next != null){
                next = next.next;
            }
            next.next = orderByEntity;
        }
        return this;
    }

    /**
     * 通过函数表达式构建分组
     * @param columns 列名的函数表达式参数
     * @return QueryEntity
     * @param <T> 泛型
     */
    @SafeVarargs
    public final <T> QueryEntity group(CustomFunction<T,?>... columns){
        List<String> fieldNames = new ArrayList<>();
        for (CustomFunction<T, ?> column : columns) {
            String fieldName = ColumnUtil.getFieldName(column);
            fieldNames.add(fieldName);
        }
        if (this.groupBy == null){
            this.groupBy = new GroupByEntity(fieldNames);
        } else {
            this.groupBy.fieldNames.addAll(fieldNames);
        }
        return this;
    }

    /**
     * 查询对象构建having
     * @param condition 查询对象
     * @return QueryEntity
     * @param <T> 泛型
     */
    public <T> QueryEntity having(Condition condition){
        if (this.groupBy == null){
            throw new RuntimeException("group is not exist");
        }
        this.groupBy.having(condition);
        return this;
    }

    public <T, R> QueryEntity on(CustomFunction<T, ?> c, CustomFunction<R, ?> v) {
        this.tableNext.condition = new Condition().condition(QueryEnum.EQUALS,c,v);
        // 替换
        return this;
    }

    public QueryEntity on(Condition condition) {
        this.tableNext.condition = condition;
        return this;
    }

    public <T> QueryEntity leftJoin(Class<T> clazz) {
        this.tableNext.nextType = 1;
        from(clazz);
        return this;
    }

    public <T> QueryEntity rightJoin(Class<T> clazz) {
        this.tableNext.nextType = 2;
        from(clazz);
        return this;
    }

    public <T> QueryEntity innerJoin(Class<T> clazz) {
        this.tableNext.nextType = 3;
        from(clazz);
        return this;
    }

    public String print() {
        return printSelect() + printFrom() + printWhere() + printGroupBy() + printOrderBy() + printLimit();
    }

    private String printSelect() {
        StringBuilder sb = new StringBuilder("select ");
        String select = String.join(",", columns);
        sb.append(select);
        return sb.toString();
    }

    private String printFrom() {
        if (null == tableRoot){
            return "";
        }
        return "\nfrom " + tableRoot.print();
    }

    private String printWhere() {
        if (this.conditionRoot == null){
            return "";
        }
        return "\nwhere " + this.conditionRoot.print();
    }

    private String printOrderBy(){
        if (this.orderBy == null){
            return "";
        }
        return "\norder by " + this.orderBy.print();
    }

    private String printGroupBy(){
        if (this.groupBy == null){
            return "";
        }
        return "\ngroup by " + this.groupBy.print();
    }

    private String printLimit(){
        if (limit == null){
            return "";
        }
        if (limit.pageNum == -1 && limit.pageNo == -1){
            return "";
        }
        return "\nlimit " + this.limit.print();
    }


    private void addFields(Field[] fields, String tbName) {
        for (Field declaredField : fields) {
            addField(declaredField, tbName);
        }
    }

    private void addField(Field field, String tbName) {
        StringBuilder fieldName = new StringBuilder();
        boolean exception = false;
        try {
            Class.forName("javax.persistence.Transient");
            Transient transientF = field.getAnnotation(Transient.class);
            if (null != transientF) {
                return;
            }
        } catch (ClassNotFoundException ignored) {
            exception = true;
        }

        if (exception) {
            try {
                Class.forName("com.baomidou.mybatisplus.annotation.TableField");
                TableField tableField = field.getAnnotation(TableField.class);
                if (tableField != null && !tableField.exist()) {
                    return;
                }
            } catch (ClassNotFoundException ignored) {
            }
        }

        if (tbName == null) {
            TargetField targetField = field.getAnnotation(TargetField.class);
            if (null == targetField) {
                return;
            }
            Class<?> entity = targetField.entity();
            String targetFieldName = targetField.name();
            String tableName = ColumnUtil.getTableName(entity);
            if (tableName != null) {
                fieldName.append(tableName).append(".");
            }
            fieldName.append(StringUtil.convertCamelToSnake(targetFieldName)).append(" ").append(field.getName());
            columns.add(fieldName.toString());
        } else {
            columns.add(tbName + "." + StringUtil.convertCamelToSnake(field.getName()) + " " + field.getName());
        }
    }


    @Override
    public String toString(){
        return "(" + this.print() + ")";
    }
}
