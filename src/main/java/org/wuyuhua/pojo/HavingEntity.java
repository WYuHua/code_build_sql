package org.wuyuhua.pojo;

/**
 * <p>分组条件实体</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/4/26 17:57
 */

public class HavingEntity {

    private final Condition condition;

    protected HavingEntity next;

    public HavingEntity(Condition condition) {
        this.condition = condition;
    }

    public String print(){
        StringBuilder sb = new StringBuilder("having ");
        if (condition == null){
            return "";
        }
        sb.append(condition.print());
        HavingEntity index = next;
        while (index != null){
            sb.append(index.condition.print());
            index = index.next;
        }
        return sb.toString();
    }
}
