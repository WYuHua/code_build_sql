package org.wuyuhua.pojo;

/**
 * <p>分页条件实体</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/4/26 17:14
 */

public class LimitEntity {

    protected int pageNo = -1;
    protected int pageNum = -1;

    public LimitEntity(int pageNo, int pageNum) {
        this.pageNo = pageNo;
        this.pageNum = pageNum;
    }

    public String print() {
        if (pageNum == -1){
            return pageNo + "";
        } else {
            return pageNo + "," + pageNum;
        }
    }
}
