package org.wuyuhua.inter;

import java.io.Serializable;
import java.util.function.Function;

/**
 * @author 伍玉华
 */
@FunctionalInterface
public interface CustomFunction<T,R> extends Function<T,R> , Serializable {
}
