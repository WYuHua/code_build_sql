package org.wuyuhua.utils;

/**
 * <p>对象操作工具类</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/4/25 10:02
 */

public class StringUtil {

    public static boolean isEmpty(String str){
        return null == str || str.isEmpty();
    }

    public static String convertCamelToSnake(String camelCaseString) {
        StringBuilder snakeCaseBuilder = new StringBuilder();
        char[] charArray = camelCaseString.toCharArray();

        for (char ch : charArray) {
            // 如果是大写字母，则在其前面加下划线并转换为小写
            if (Character.isUpperCase(ch)) {
                snakeCaseBuilder.append("_").append(Character.toLowerCase(ch));
            } else {
                snakeCaseBuilder.append(ch);
            }
        }

        // 如果第一个字符是下划线，则去除它
        if (snakeCaseBuilder.length() > 0 && snakeCaseBuilder.charAt(0) == '_') {
            snakeCaseBuilder.deleteCharAt(0);
        }

        return snakeCaseBuilder.toString();
    }
}
