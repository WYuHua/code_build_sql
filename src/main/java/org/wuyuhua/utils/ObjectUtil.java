package org.wuyuhua.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>对象操作工具类</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/4/25 10:05
 */
@Slf4j
public class ObjectUtil {

    /**
     * 判断是否为数值类
     * @param obj 对象
     * @return true or false
     */
    public static boolean isNotNumber(Object obj){
        return !(obj instanceof Number);
    }
}
