package org.wuyuhua.utils;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import org.wuyuhua.inter.CustomFunction;

import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * <p>列工具类</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/4/24 19:36
 */

public class ColumnUtil {

    public static <T> CustomFunctionInfo getFunctionInfo(CustomFunction<T, ?> function) {
        CustomFunctionInfo customFunctionInfo = new CustomFunctionInfo();
        SerializedLambda serializedLambda = getSerializedLambda(function);
        // 从lambda信息取出method、name、class等
        String fieldName = serializedLambda.getImplMethodName().substring("get".length());
        fieldName = fieldName.replaceFirst(fieldName.charAt(0) + "", (fieldName.charAt(0) + "").toLowerCase());
        Class<?> clazz;
        try {
            clazz = Class.forName(serializedLambda.getImplClass().replace("/", "."));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        Field field;
        try {
            field = Class.forName(serializedLambda.getImplClass().replace("/", ".")).getDeclaredField(fieldName);
        } catch (NoSuchFieldException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        customFunctionInfo.clazz = clazz;
        customFunctionInfo.field = field;
        return customFunctionInfo;
    }

    private static <T> SerializedLambda getSerializedLambda(CustomFunction<T, ?> function) {
        Method writeReplaceMethod;
        try {
            writeReplaceMethod = function.getClass().getDeclaredMethod("writeReplace");
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        writeReplaceMethod.setAccessible(true);
        SerializedLambda serializedLambda;
        try {
            serializedLambda = (SerializedLambda) writeReplaceMethod.invoke(function);
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return serializedLambda;
    }

    /**
     * 通过函数获取类和列
     * @param function 函数
     * @return 类.列
     * @param <T> 泛型
     */
    public static <T> String getColumnName(CustomFunction<T,?> function){
        ColumnUtil.CustomFunctionInfo functionInfo = ColumnUtil.getFunctionInfo(function);
        Field field = functionInfo.getField();
        Class<?> clazz = functionInfo.getClazz();
        Table annotation = clazz.getAnnotation(Table.class);
        if (null == annotation) {
            throw new RuntimeException("class not is TableEntity");
        }
        String tableName = annotation.name();
        if (null == tableName || tableName.isEmpty()) {
            tableName = clazz.getSimpleName();
        }
        return tableName + "." + field.getName();
    }

    @Getter
    public static class CustomFunctionInfo {
        Field field;
        Class<?> clazz;
    }

    public static String getTableName(Class<?> clazz){
        StringBuilder tbName = new StringBuilder();
        String schema = null;
        String tableName = null;
        // 检查Table类是否存在
        try {
            Class.forName("javax.persistence.Table");
            Table annotation = clazz.getAnnotation(Table.class);
            if (null != annotation){
                schema = annotation.schema();
                tableName = annotation.name();
            }
            if (null == annotation){
                throw new RuntimeException();
            }
        } catch (ClassNotFoundException | RuntimeException e) {
            TableName tbAnnotation = clazz.getAnnotation(TableName.class);
            if (null == tbAnnotation){
                return null;
            } else {
                schema = tbAnnotation.schema();
                tableName = tbAnnotation.value();
            }
        }
        if (!StringUtil.isEmpty(schema)){
            tbName.append(schema).append(".");
        }
        if (StringUtil.isEmpty(tableName)){
            tableName = clazz.getSimpleName();
        }
        tbName.append(tableName);
        return tbName.toString();
    }

    public static <T> String getFieldName(CustomFunction<T, ?> function){
        if (function == null){
            return "";
        }
        ColumnUtil.CustomFunctionInfo functionInfo = ColumnUtil.getFunctionInfo(function);
        Class<?> clazz = functionInfo.getClazz();
        String tableName = ColumnUtil.getTableName(clazz);
        Field field = functionInfo.getField();
        String fieldName = StringUtil.convertCamelToSnake(field.getName());
        return tableName != null?tableName + "." + fieldName:fieldName;
    }
}
