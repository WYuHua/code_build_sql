package org.wuyuhua.test;

import org.wuyuhua.pojo.Condition;
import org.wuyuhua.pojo.QueryEntity;
import org.wuyuhua.test.pojo.TRole;
import org.wuyuhua.test.pojo.TUser;
import org.wuyuhua.test.pojo.TUserDto;
import org.wuyuhua.test.pojo.TUserRole;

public class TestMain {
    public static void main(String[] args) {
        Condition condition = new Condition()
                .eq(TUser::getSex,"男")
                .child(new Condition().eq(TUser::getUserName,"张三").eq(TUser::getAge,18))
                .or()
                .child(new Condition().eq(TUser::getUserName,"李四").eq(TUser::getAge,19).or().neq(TUser::getAge,17));
        QueryEntity queryEntity = new QueryEntity()
                .select(TUserDto.class)
                .from(TUser.class)
                .leftJoin(TUserRole.class).on(TUser::getId,TUserRole::getUserId)
                .leftJoin(TRole.class).on(TUserRole::getRoleId,TRole::getId)
                .where(condition);
        System.out.println(queryEntity.print());
    }
}
