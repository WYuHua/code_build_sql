package org.wuyuhua.test.pojo;

import lombok.Data;

import javax.persistence.Table;

@Data
@Table(name = "t_user_role")
public class TUserRole {
    private Long id;
    private Long userId;
    private Long roleId;
}
