package org.wuyuhua.test.pojo;

import lombok.Data;
import org.wuyuhua.annotation.TargetField;

/**
 * 自定义用户信息结果返回对象
 */
@Data
public class TUserDto {

    @TargetField(entity = TUser.class,name = "id")
    private Long userId;
    @TargetField(entity = TUser.class,name = "userName")
    private String username;
    @TargetField(entity = TUser.class,name = "passWord")
    private String password;
    @TargetField(entity = TRole.class,name = "code")
    private String roleCode;
    private String idNo;
}
