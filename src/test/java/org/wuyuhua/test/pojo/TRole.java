package org.wuyuhua.test.pojo;

import lombok.Data;

import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "t_role")
public class TRole {

    private Long id;
    private String name;
    private String code;
    private Date createTime;
}
