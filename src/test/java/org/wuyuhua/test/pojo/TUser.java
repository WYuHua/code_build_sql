package org.wuyuhua.test.pojo;

import lombok.Data;

import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "t_user")
public class TUser {

    private Long id;
    private String userName;
    private String passWord;
    private String idNo;
    private Integer age;
    private String sex;
    private Date birth;
}
